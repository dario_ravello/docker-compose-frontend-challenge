﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using server.Models;

namespace server.Utils
{
    public class WaterBottleUtil
    {
        public static string getShortestPath(int a, int b, int target)
        {
            // Map is used to store the states, every
            // state is hashed to binary value to
            // indicate either that state is visited
            // before or not
            List<BottleState> result = new List<BottleState>();
            Dictionary<Tuple<int,int>, int> m = new Dictionary<Tuple<int,int> ,int>(); 
            bool isSolvable = false;
            List<Tuple<int,int>> path = new List<Tuple<int,int>>();
            // Queue to maintain states
            List<Tuple<int,int>> q = new List<Tuple<int,int>>();
         
            // Initializing with initial state
            q.Add(new Tuple<int,int>(0, 0));
      
            while (q.Count > 0)
            {
                // Current state
                Tuple<int, int> u = q[0];
                 
                // Pop off used state
                q.RemoveAt(0);
          
                // If this state is already visited
                if (m.ContainsKey(u) && m[u] == 1)
                    continue;
          
                // Doesn't met jug constraints
                if ((u.Item1 > a || u.Item2 > b ||
                    u.Item1 < 0 || u.Item2 < 0))
                    continue;
          
                // Filling the vector for constructing
                // the solution path
                path.Add(u);
          
                // Marking current state as visited
                m[u] = 1;
          
                // If we reach solution state, put ans=1
                if (u.Item1 == target || u.Item2 == target)
                {
                    isSolvable = true;
                     
                    if (u.Item1 == target)
                    {
                        if (u.Item2 != 0)
          
                            // Fill final state
                            path.Add(new Tuple<int, int>(u.Item1, 0));
                    }
                    else
                    {
                        if (u.Item1 != 0)
          
                            // Fill final state
                            path.Add(new Tuple<int, int>(0, u.Item2));
                    }
          
                    // Print the solution path
                    int sz = path.Count;
                    for(int i = 0; i < sz; i++)
                        result.Add(new BottleState(path[i].Item1, getBottleStateName(path[i].Item1, a), path[i].Item2, getBottleStateName(path[i].Item2, b)));
                    break;
                }
          
                // If we have not reached final state
                // then, start developing intermediate
                // states to reach solution state
                // Fill Jug2
                q.Add(new Tuple<int, int>(u.Item1, b));
                 
                // Fill Jug1
                q.Add(new Tuple<int, int>(a, u.Item2));

                int maxValue = Math.Max(a, b);
                for(int ap = 0; ap <= maxValue; ap++)
                {
                     
                    // Pour amount ap from Jug2 to Jug1
                    int c = u.Item1 + ap;
                    int d = u.Item2 - ap;
          
                    // Check if this state is possible or not
                    if (c == a || (d == 0 && d >= 0))
                        q.Add(new Tuple<int, int>(c, d));
          
                    // Pour amount ap from Jug 1 to Jug2
                    c = u.Item1 - ap;
                    d = u.Item2 + ap;
          
                    // Check if this state is possible or not
                    if ((c == 0 && c >= 0) || d == b)
                        q.Add(new Tuple<int, int>(c, d));
                }
                 
                // Empty Jug2
                q.Add(new Tuple<int, int>(a, 0));
                 
                // Empty Jug1
                q.Add(new Tuple<int, int>(0, b));
            }
          
            // No, solution exists if ans=0
            if (!isSolvable)
                result = null;
            
            return JsonConvert.SerializeObject(result);
        }
        
        public static string validateBottles(int bottleX, int bottleY, int bottleZ) {
        // Validate bottles
        if (bottleX.ToString().Length <= 0 || bottleX.ToString().Length > 15) {
          return "The quantity of the bottle X must be between 0 and 999999999999999";
        }
        if (bottleY.ToString().Length <= 0 || bottleY.ToString().Length > 15) {
          return "The quantity of the bottle Y must be between 0 and 999999999999999";
        }
        if (bottleZ.ToString().Length <= 0 || bottleZ.ToString().Length > 15) {
          return "The quantity of the bottle Z must be between 0 and 999999999999999";
        }

        // Validate measure
        int maxValue = Math.Max(bottleX, bottleY);
        if (bottleZ >= maxValue)
        {
          return "The quantity of the bottle z must be greater than those of X and Y";
        }

        // Validate if there is a solution
        int gcdNumber = gcd_two_numbers(bottleX, bottleY);
        Boolean isMultiple = (bottleZ % gcdNumber) == 0;

        if (gcdNumber == 0 || !isMultiple)
        {
          return "No Solution";
        }

        // Return valid
        return "";
      }

        private static string getBottleStateName(int quantity, int total)
        {
            if (quantity == 0)
                return "Empty";

            if (quantity == total)
                return "Full";

            if (quantity < total)
                return "Partial Full";

            return "Error";
        }
        private static int  gcd_two_numbers(int number1, int number2) {
            int a = Math.Abs(number1);
            int b = Math.Abs(number2);
            while (a != 0 && b != 0)
            {
              if (a > b)
                a %= b;
              else
                b %= a;
            }

            return a | b;
        }
  }
}