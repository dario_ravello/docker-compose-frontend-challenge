﻿namespace server.Models
{
    public class BottleState
    {
        public int bottleX { get; set; }
        public string bottleXState { get; set; }
        public int bottleY { get; set; }
        public string bottleYState { get; set; }

        public BottleState(int bottleX, string bottleXState, int bottleY, string bottleYState)
        {
            this.bottleX = bottleX;
            this.bottleXState = bottleXState;
            this.bottleY = bottleY;
            this.bottleYState = bottleYState;
        }

        public BottleState()
        {
            bottleX = 0;
            bottleXState = "Empty";
            bottleY = 0;
            bottleYState = "Empty";
        }
    }
}