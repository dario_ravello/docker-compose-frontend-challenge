﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using server.Models;
using server.Utils;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WaterBottleController : ControllerBase
    {
        // GET api/waterbottle/bottleX/bottleY/bottleZ
        [HttpGet("{bottleX}/{bottleY}/{bottleZ}")]
        public ActionResult<string> Get(int bottleX,int bottleY,int bottleZ)
        {
            string validationMessage = WaterBottleUtil.validateBottles(bottleX, bottleY, bottleZ);

            if (!String.IsNullOrEmpty(validationMessage)) {
                return validationMessage;
            }
            
            string path = WaterBottleUtil.getShortestPath(bottleX, bottleY, bottleZ);

            return path;
        }
    }
}
